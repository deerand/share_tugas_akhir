/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.30-MariaDB : Database - tadwi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tadwi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tadwi`;

/*Table structure for table `data_objek` */

DROP TABLE IF EXISTS `data_objek`;

CREATE TABLE `data_objek` (
  `id` int(5) NOT NULL,
  `kecamatan` varchar(20) NOT NULL,
  `nilai_mop` int(11) NOT NULL,
  `nilai_kondom` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_objek` */

insert  into `data_objek`(`id`,`kecamatan`,`nilai_mop`,`nilai_kondom`) values (1,'Sukajadi',31,479),(2,'Bandung Wetan',73,73),(3,'Coblong',23,670),(4,'Gedebage',102,151),(5,'Cibeunying Kidul',53,281),(6,'Andir',19,160),(7,'Cicendo',33,219),(8,'Lengkong',16,201),(9,'Regol',47,279),(10,'Cibeunying Kaler',27,111),(11,'Astana Anyar',48,134),(12,'Cidadap',7,121),(13,'Sumur Bandung',2,153),(14,'Arcamanik',24,111),(15,'Mandalajati',10,207),(16,'Panyileukan',3,198),(17,'Bojongloa Kaler',56,257),(18,'Bandung Kulon',10,131),(19,'Sukasari',21,115),(20,'Antapani',31,226),(21,'Batununggal',18,208),(22,'Rancasari',11,239),(23,'Cibiru',33,135),(24,'Cinambo',9,61),(25,'Bandung Kidul',7,122),(26,'Kiaracondong',20,460),(27,'Babakan Ciparay',33,103),(28,'Buahbatu',26,160),(29,'Bojongloa Kidul',32,88),(30,'Ujung Berung',58,486);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
