<?php
//Admin
//User
include './templateadmin/templateadmin.php';
include './templateadmin/menu.php';
include './templateadmin/isi.php';

$tampil = $template;
$pg=isset($_GET['pg'])?$_GET['pg']:"";
switch ($pg)
{
//-----------------Index-----------------
case 'home':
include './index.php';
break;
//-----------------CRUD FORM OBJEK-------
case 'form_data_objek':
include './templateadmin/page/form_data_objek.php';
break;
//------------------ITERASI--------------
case 'iterasi_kmeans':
include './templateadmin/page/iterasi_kmeans.php';
break;
case 'iterasi_kmeans_lanjut':
include './templateadmin/page/iterasi_kmeans_lanjut.php';
break;
case 'iterasi_kmeans_hasil':
include './templateadmin/page/iterasi_kmeans_hasil.php';
break;
//------------------Chart---------------
case 'hasil_chart_iterasi':
include './templateadmin/page/hasil_chart_iterasi.php';
break;

}

$tampil = str_replace("<!--menu-->",$menu,$tampil);
$tampil = str_replace("<!--isi-->",$isi,$tampil);

echo $tampil;
?>