/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.30-MariaDB : Database - tadwi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tadwi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tadwi`;

/*Table structure for table `data_objek` */

DROP TABLE IF EXISTS `data_objek`;

CREATE TABLE `data_objek` (
  `id` int(5) NOT NULL,
  `kecamatan` varchar(20) NOT NULL,
  `nilai_mop` int(11) NOT NULL,
  `nilai_kondom` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_objek` */

insert  into `data_objek`(`id`,`kecamatan`,`nilai_mop`,`nilai_kondom`) values (1,'Cisalak',303,186),(2,'Sukasari',18,62),(3,'Pamanukan',4,64),(4,'Kasomalang',47,10),(5,'Cijambe',276,10),(6,'Jalancagak',57,0),(7,'Ciater',6,10),(8,'Serang Panjang',45,32),(9,'Sagalaherang',68,36),(10,'Pagaden',29,107),(11,'Pagaden Barat',21,49),(12,'Ciasem',473,385),(13,'Subang',125,147),(14,'Dawuan',24,31),(15,'Cibogo',20,57),(16,'Cipunagara',185,72),(17,'Purwadadi',47,53),(18,'Cikaum',76,0),(19,'Kalijati',34,139),(20,'Cipeundeuy',27,67),(21,'Tanjung Siang',96,249),(22,'Compreng',84,193),(23,'Pusakajaya',101,138),(24,'Blanakan',85,231),(25,'Binong',23,192),(26,'Tambakdahan',10,232),(27,'Patokbeusi',62,85),(28,'Pabuaran',64,100),(29,'Pusakanagara',117,269),(30,'Legonkulon',57,191);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
