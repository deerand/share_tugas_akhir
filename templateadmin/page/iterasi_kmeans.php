<?php
$table = "
<div class = 'box box-warning'>
    <div class='box-header with-border'>
        <h3 class='box-title'>Tugas Akhir</h3>
    </div>
</div>
<div class='box box-warning'>
    <div class='box-header with-border'>
        <h3 class='box-title'>Data</h3>
    </div>
        <h4 class='box-title'>TUGAS AKHIR</h4>
        <a href='?pg=iterasi_kmeans_lanjut' class='btn btn-primary'>ITERASI SELANJUTNYA</a>
        <h3 class='box-title'>Iterasi 1</h3>
        <table class='table table-hover' cellspacing='0' width='100%'>
        <tr align='center'>
            <td rowspan='2'>KECAMATAN</td>
            <td rowspan='2'>NILAI MOP</td>
            <td rowspan='2'>NILAI KONDOM</td>
            <td colspan='2'>Centroid 1</td>
            <td colspan='2'>Centroid 2</td>
            <td colspan='2'>Centroid 3</td>
            <td rowspan='2'>C1</td>
            <td rowspan='2'>C2</td>
            <td rowspan='2'>C3</td>
        </tr>
        <tr align='center'>
            <td>4</td>
            <td>64</td>            
            <td>84</td>
            <td>193</td>
            <td>473</td>
            <td>385</td>
        </tr>";
    ?>
    <?php 
        $con = mysqli_connect('localhost','root','') or die ('Xampp Belum di Nyalakan');
        mysqli_select_db($con,'tadwi') or die ('Tidak ada database');        
        $c1a = 4;
        $c1b = 64;
        
        $c2a = 84;
        $c2b = 193;
        
        $c3a = 473;
        $c3b = 385;

        $c1a_b = "";
        $c1b_b = "";
        $c1c_b = "";
        
        $c2a_b = "";
        $c2b_b = "";
        $c2c_b = "";
        
        $c3a_b = "";
        $c3b_b = "";
        $c3c_b = "";
        
        $hc1=0;
        $hc2=0;
        $hc3=0;
        
        $no=0;
        $noplus=1;
        
        $arr_c1 = array();
        $arr_c2 = array();
        $arr_c3 = array();
        
        $arr_c1_temp = array();
        $arr_c2_temp = array();
        $arr_c3_temp = array();

        $truncate_centroid_temp = "TRUNCATE TABLE centroid_temp";
        $sql_centroid = mysqli_query($con,$truncate_centroid_temp);

        $truncate_hasil_centroid = "TRUNCATE TABLE hasil_centroid";
        $sql_centroid = mysqli_query($con,$truncate_hasil_centroid);
        
        $query_centroid_awal = "INSERT INTO hasil_centroid(c1a,c1b,c2a,c2b,c3a,c3b) values('".$c1a."','".$c1b."','".$c2a."','".$c2b."','".$c3a."','".$c3b."')";
        $sql_query_centroid_awal = mysqli_query($con,$query_centroid_awal);
        

        $getData = "SELECT * FROM data_objek";
        $sql_getData = mysqli_query($con,$getData);

        $queryforcentroid = "SELECT * FROM data_objek ORDER BY id ASC LIMIT 1";
        $sqlforcentroid = mysqli_query($con,$queryforcentroid);
        while($getdataforcentroid = mysqli_fetch_array($sqlforcentroid))
        {
          $getidforcentroid = $getdataforcentroid['id'];
        }
        $convertstrtointcentroid = (int)$getidforcentroid;
        
        $data_objek="";
        $c="";

        while($data=mysqli_fetch_array($sql_getData))
        {
            $test1 = $convertstrtointcentroid++;
            
            $hc1 = sqrt(pow(($data['nilai_mop']-$c1a),2) + pow(($data['nilai_kondom']-$c1b),2));
            $hc2 = sqrt(pow(($data['nilai_mop']-$c2a),2) + pow(($data['nilai_kondom']-$c2b),2));
            $hc3 = sqrt(pow(($data['nilai_mop']-$c3a),2) + pow(($data['nilai_kondom']-$c3b),2));
            if($hc1<=$hc2)
            {
              if($hc1<=$hc3)
              {
                $arr_c1[$no] = 1;
              }
              else
              {
                $arr_c1[$no] = '0';
              }
            }
            else
            {
              $arr_c1[$no] = '0';
            }
            
            if($hc2<=$hc1)
            {
                if($hc2<=$hc3)
                {
                    $arr_c2[$no] = 1;
                }
                else
                {
                    $arr_c2[$no] = '0';
                }
                }else
                {
                    $arr_c2[$no] = '0';
                }
            
                if($hc3<=$hc1)
                {
            if($hc3<=$hc2)
                {
                    $arr_c3[$no] = 1;
                }
                else
                {
                    $arr_c3[$no] = '0';
                }
            }
            else
            {
              $arr_c3[$no] = '0';
            }
            
            $arr_c1_temp[$no] = $data['nilai_mop'];
            $arr_c2_temp[$no] = $data['nilai_kondom'];

            $warna1="";
            $warna2="";
            $warna3="";
            if($arr_c1[$no]==1){$warna1='#7eff54';} else{$warna1='#ccc';}
            if($arr_c2[$no]==1){$warna2='YELLOW';} else{$warna2='#ccc';}
            if($arr_c3[$no]==1){$warna3='RED';} else{$warna3='#ccc';}
            $data_objek .="
            <TR align='center'>
                <TD>".$data['kecamatan']."</TD>
                <TD>".$data['nilai_mop']."</TD>
                <TD>".$data['nilai_kondom']."</TD>
                <TD colspan='2'>".$hc1."</TD>
                <TD colspan='2'>".$hc2."</TD>
                <TD colspan='2'>".$hc3."</TD>
                <td bgcolor=".$warna1.">".$arr_c1[$no]."</td>
                <td bgcolor=".$warna2.">".$arr_c2[$no]."</td>
                <td bgcolor=".$warna3.">".$arr_c3[$no]."</td>
            </TR>";    
            $q = "INSERT INTO centroid_temp(id_kecamatan,iterasi,c1,c2,c3) values($test1,1,'".$arr_c1[$no]."','".$arr_c2[$no]."','".$arr_c3[$no]."')";
            $sql = mysqli_query($con,$q);
            $no++;
    }
    $jum = 0;
    $arr = array();
    for($i=0;$i<count($arr_c1);$i++)
    {
      $arr[$i] = $arr_c1_temp[$i]*$arr_c1[$i];
      if($arr_c1[$i]==1)
      {
        $jum++;
      }
    }
    $c1a_b = array_sum($arr)/$jum;
    
    //centroid baru 1.b
    $jum = 0;
    $arr = array();
    for($i=0;$i<count($arr_c1);$i++)
    {
      $arr[$i] = $arr_c2_temp[$i]*$arr_c1[$i];
      if($arr_c1[$i]==1)
      {
        $jum++;
      }
    }
    $c1b_b = array_sum($arr)/$jum;
        
    
    //centroid baru 2.a
    $jum = 0;
    $arr = array();
    for($i=0;$i<count($arr_c2);$i++)
    {
      $arr[$i] = $arr_c1_temp[$i]*$arr_c2[$i];
      if($arr_c2[$i]==1)
      {
        $jum++;
      }
    }
    
    $c2a_b = array_sum($arr)/$jum;

    //centroid baru 2.b
    $jum = 0;
    $arr = array();
    for($i=0;$i<count($arr_c2);$i++)
    {
      $arr[$i] = $arr_c2_temp[$i]*$arr_c2[$i];
      if($arr_c2[$i]==1)
      {
        $jum++;
      }
    }
    $c2b_b = array_sum($arr)/$jum;
    
    //centroid baru 3.a
    $jum = 0;
    $arr = array();
    for($i=0;$i<count($arr_c3);$i++)
    {
      $arr[$i] = $arr_c1_temp[$i]*$arr_c3[$i];
      if($arr_c3[$i]==1)
      {
        $jum++;
      }
    }
    $c3a_b = array_sum($arr)/$jum;
    
    //centroid baru 3.b
    $jum = 0;
    $arr = array();
    for($i=0;$i<count($arr_c3);$i++)
    {
      $arr[$i] = $arr_c2_temp[$i]*$arr_c3[$i];
      if($arr_c3[$i]==1)
      {
        $jum++;
      }
    }
    $c3b_b = array_sum($arr)/$jum;
    
    $q2 = "INSERT INTO hasil_centroid(c1a,c1b,c2a,c2b,c3a,c3b) values('".$c1a_b."','".$c1b_b."','".$c2a_b."','".$c2b_b."','".$c3a_b."','".$c3b_b."')";
    $sql2 = mysqli_query($con,$q2);

$isi = "$table $data_objek</table></div>";
?>