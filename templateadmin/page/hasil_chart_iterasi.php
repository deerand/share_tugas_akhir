<?php
$con = mysqli_connect('localhost','root','') or die ('Xampp Belum di Nyalakan');
mysqli_select_db($con,'tadwi') or die ('Tidak ada database');
$desain = "
<!-- DONUT CHART -->
<div class='box box-warning'>
  <div class='box-header with-border'>
    <h3 class='box-title'>Centroid</h3>

    <div class='box-tools pull-right'>
      <button type='button' class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-minus'></i>
      </button>
      <button type='button' class='btn btn-box-tool' data-widget='remove'><i class='fa fa-times'></i></button>
    </div>
  </div>
  <div class='box-body'>
  <form role='form' method='POST' autocomplete='off'>
  <div class='form-group'>
  <label>Per Iterasi</label>
    <input type='number' name='cek_per_iterasi' class='form-control' placeholder='Enter ...'>    
  </div>
  <div class='form-group'>
  <label>Label Per Iterasi</label>
    <input type='text' name='disabled_iterasi' class='form-control' disabled>
  </div>


    <button type='submit' id='button_per_iterasi' class='btn btn-primary'>Cek Per Iterasi</button>
    <button type='submit' id='reset_iterasi' class='btn btn-danger'>Reset</button>
  </form>
    <canvas id='pieChart' style='height:250px'></canvas>
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->   
";
$isi = "$desain";
?>