<?php
$menu = "
<li class='treeview'>
    <a href='#'>
        <i class='fa fa-edit'></i> <span>Forms</span>
        <span class='pull-right-container'>
            <i class='fa fa-angle-left pull-right'></i>
        </span>
    </a>
    <ul class='treeview-menu'>
        <li><a href='?pg=form_data_objek'><i class='fa fa-circle-o'></i>Form Data Objek</a></li>
        <li><a href='?pg=iterasi_kmeans'><i class='fa fa-circle-o'></i>Iterasi K-Means</a></li>
        <li><a href='?pg=hasil_chart_iterasi'><i class='fa fa-circle-o'></i>Chart Iterasi</a></li>
    </ul>
</li>";
?>
