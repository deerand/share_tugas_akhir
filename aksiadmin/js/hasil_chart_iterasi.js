$(document).ready(function(){
    var ctx = document.getElementById("pieChart").getContext('2d');
    var options     = {
        //Boolean - Whether we should show a stroke on each segment
        segmentShowStroke    : true,
        //String - The colour of each segment stroke
        segmentStrokeColor   : '#fff',
        //Number - The width of each segment stroke
        segmentStrokeWidth   : 2,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 50, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps       : 100,
        //String - Animation easing effect
        animationEasing      : 'easeOutBounce',
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate        : true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale         : false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive           : true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio  : true,
        //String - A legend template
        legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
  
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data : {
            datasets: [{
                data: [0],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                ],
                hoverBackgroundColor : [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                ],
                borderWidth: 1
            }],
        
            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: ['Total'
            ],

        },
        
    });
    $.ajax({
        url : './aksiadmin/data/json_chart.php',
        type : 'GET',
        dataType : 'JSON',
        success : function(data)
        {            
            var mapped = Object.keys( data  ).map(function( uid ){
                return parseInt((data[uid].uid = uid) && data[uid]);
            });

            myChart.data.datasets.map(function(o){
                o.data = mapped;
                return o;
            })
            myChart.data.labels = ["Total Centroid 1","Total Centoid 2","Total Centroid 3"];        
            myChart.update();
        }
    });
    $('#button_per_iterasi').click(function(e){
        
        e.preventDefault();
        var cek_per_iterasi = $("input[name='cek_per_iterasi']").val();
        var disabled_iterasi = $("input[name='disabled_iterasi']").val(cek_per_iterasi);        
        $.ajax({
            url : './aksiadmin/cek_per_iterasi.php',
            type : 'POST',
            dataType : 'JSON',
            data : {
                cek_per_iterasi : cek_per_iterasi
            },
            success : function(data)
            {
                var array_data_cent = Object.keys(data).map(function(k) { return data[k] });                
                if(array_data_cent[0]==null && array_data_cent[1]==null && array_data_cent[2]==null)
                {
                    var iterasi_kosong = $("input[name='disabled_iterasi']").val('Data Kosong');        
                    myChart.data.labels = ["Data Kosong"];        
                    
                    myChart.data.datasets.map(function(o){
                        o.data = array_data_cent;
                        return o;
                    })
                    myChart.update();
                }
                else
                {

                myChart.data.datasets.map(function(o){
                    o.data = array_data_cent;
                    return o;
                })
                
                myChart.data.labels = ["Per Centroid 1","Per Centoid 2","Per Centroid 3"];        
                myChart.update();

            }
            }
        })
    })
    $('#reset_iterasi').click(function(e){
        var reset = $("input[name='cek_per_iterasi']").val('');
        var disabled_iterasi = $("input[name='disabled_iterasi']").val('');
        e.preventDefault();
        $.ajax({    
            url : './aksiadmin/data/json_chart.php',
            type : 'GET',
            dataType : 'JSON',
            success : function(data)
            {            
                var mapped = Object.keys( data  ).map(function( uid ){
                    return parseInt((data[uid].uid = uid) && data[uid]);
                });
    
                myChart.data.datasets.map(function(o){
                    o.data = mapped;
                    return o;
                })
                myChart.data.labels = ["Total Centroid 1","Total Centoid 2","Total Centroid 3"];        
                myChart.update();
            }
        });        
    });
});