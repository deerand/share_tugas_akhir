$(document).ready(function(){
    $('#datatable_data_objek').DataTable( {
        "language" : {
            "sProcessing":   "Sedang memproses...",
            "sLengthMenu":   "Tampilkan _MENU_ Entri",
            "sZeroRecords":  "Tidak ditemukan data yang sesuai/Tidak ada data",
            "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix":  "",
            "sSearch":       "Cari:",
            "sUrl":          "",
            "oPaginate": {
            "sFirst":    "Pertama",
            "sPrevious": "Sebelumnya",
            "sNext":     "Selanjutnya",
            "sLast":     "Terakhir"
            }
          },
        "processing": true,
        "serverSide": true,
        "ajax": "./aksiadmin/data/json_objek.php",
        "columns": [
            { "data" : "no"},
            { "data" : "kecamatan" },
            { "data" : "nilai_mop" },
            { "data" : "nilai_kondom" },
            {data: 'id', name: 'id',
            render:function(data,type,full,meta){
              return "<button class =\"btn btn-danger\" id=\"delete_objek\" data-id=\""+data+"\">Delete</button>\
              <button class =\"btn btn-primary\" id=\"edit_objek\" data-id_edit=\""+data+"\">Edit</button>";
            }},
        ],
        "order": [[1, 'asc']]
    });
    $('#tambah_data_objek').click(function(e){
        e.preventDefault();
        var id_tambah = $("input[name='id_tambah']").val();        
        var kecamatan = $("input[name='kecamatan']").val();
        var nilai_mop = $("input[name='nilai_mop']").val();
        var nilai_kondom = $("input[name='nilai_kondom']").val();
    if(kecamatan !='' && nilai_mop !='' && nilai_kondom !='')
    {
    $.ajax({
        type : 'POST',
        url : './aksiadmin/tambah_data_objek.php',
        data : {
            incid : id_tambah,
            kecamatan:kecamatan,
            nilai_mop : nilai_mop,
            nilai_kondom : nilai_kondom,
        },
    }).done(function(data){
            var kecamatan = $("input[name='kecamatan']").val('');
            var nilai_mop = $("input[name='nilai_mop']").val('');
            var nilai_kondom = $("input[name='nilai_kondom']").val('');
            swal("Good job!", "Data berhasil di tambahkan", "success");            
            $('#datatable_data_objek').DataTable().ajax.reload(null, false).draw();
        })
    }else{
        swal("Fail", "Data gagal di tambah!", "error");        
    }
    })
    $("#datatable_data_objek").on('click', '#delete_objek', function (e) {
        e.preventDefault();
        var id = $(this).data("id")
        if(id !=''){

        
        $.ajax({
            type : 'POST',
            url : './aksiadmin/delete_objek.php',
            data : {
                id : id
            },
        }).done(function(data){            
            swal("Good job!", "Data berhasil di tambahkan", "success");
            $('html,body').animate({
                scrollTop: $("#datatable_data_objek").offset().top},
                'slow');
            $('#datatable_data_objek').DataTable().ajax.reload(null, false).draw();
        })
    }else{
        swal("Fail", "Data gagal di hapus!", "error");
    }
    })
    $('#datatable_data_objek').on('click','#edit_objek',function(e){
        $('html,body').animate({
            scrollTop: $("#form_objek").offset().top},
            'slow');
        e.preventDefault();
        var id = $(this).data('id_edit')
        $.ajax({
            type : 'POST',            
            url : './aksiadmin/lihat_objek_perid.php',
            data : {
                id : id
            },
            dataType : 'JSON',
            
        }).done(function(data){
            $('#tambah_data_objek').css('display','none');
            $('#submit_edit').css('display','');
            var id = $("input[name='id']").val(data.id);
            var kecamatan = $("input[name='kecamatan']").val(data.kecamatan);
            var nilai_mop = $("input[name='nilai_mop']").val(data.nilai_mop);
            var nilai_kondom = $("input[name='nilai_kondom']").val(data.nilai_kondom);            
        })
    })
    $('#submit_edit').click(function(e){
        e.preventDefault();
        var id = $('#id_update').val();
        var kecamatan = $("input[name='kecamatan']").val();
        var nilai_mop = $("input[name='nilai_mop']").val();
        var nilai_kondom = $("input[name='nilai_kondom']").val();
        
        $.ajax({
            type : 'POST',
            url : './aksiadmin/edit_data_objek.php',
            data : {
                id : id,
                kecamatan : kecamatan,
                nilai_mop : nilai_mop,
                nilai_kondom : nilai_kondom,
            }
        }).done(function(data){
            $('#tambah_data_objek').css('display','');
            $('#submit_edit').css('display','none');
            var id = $("input[name='id']").val('');
            var kecamatan = $("input[name='kecamatan']").val('');
            var nilai_mop = $("input[name='nilai_mop']").val('');
            var nilai_kondom = $("input[name='nilai_kondom']").val('');
            swal("Good job!", "Data berhasil di edit", "success");
            $('html,body').animate({
                scrollTop: $("#datatable_data_objek").offset().top},
                'slow');
            $('#datatable_data_objek').DataTable().ajax.reload(null, false).draw();
            
    
            
        })
    })
    $('#scroll_down_tambah_data').click(function(e)
    {
        $('html,body').animate({
            scrollTop: $("#form_objek").offset().top},
            'slow');
    });
    
})