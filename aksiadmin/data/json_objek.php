<?php
$con = mysqli_connect('localhost','root','') or die ('Xampp Belum di Nyalakan');
mysqli_select_db($con,'tadwi') or die ('Tidak ada database');
$requestData= $_REQUEST;
$columns = array( 
    // datatable column index  => database column name
    0 => 'no',
    1 => 'id',
    2 => 'kecamatan',
    3 => 'nilai_mop',
    4 => 'nilai_kondom',
);
// getting total number records without any search
$sql = "SELECT id,kecamatan,nilai_mop,nilai_kondom FROM data_objek";
$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT id,kecamatan,nilai_mop,nilai_kondom FROM data_objek";
    $sql.=" WHERE id LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
	$sql.=" OR kecamatan LIKE '".$requestData['search']['value']."%' ";    
	$sql.=" OR nilai_mop LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR nilai_kondom LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees"); // again run query with limit
	
	} else {	
    $sql = "SELECT id,kecamatan,nilai_mop,nilai_kondom FROM data_objek";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();
    
    $nestedData['no'] = $no++;
    $nestedData['id'] = $row["id"];
	$nestedData['kecamatan'] = $row["kecamatan"];
	$nestedData['nilai_mop'] = $row["nilai_mop"];	
	$nestedData['nilai_kondom'] = $row["nilai_kondom"];
	
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>