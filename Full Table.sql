/*
SQLyog Professional v12.09 (64 bit)
MySQL - 10.1.30-MariaDB : Database - tadwi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tadwi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tadwi`;

/*Table structure for table `centroid_temp` */

DROP TABLE IF EXISTS `centroid_temp`;

CREATE TABLE `centroid_temp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_kecamatan` int(5) DEFAULT NULL,
  `iterasi` int(11) NOT NULL,
  `c1` varchar(50) NOT NULL,
  `c2` varchar(50) NOT NULL,
  `c3` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_id_kecamatan` (`id_kecamatan`),
  CONSTRAINT `fk_id_kecamatan` FOREIGN KEY (`id_kecamatan`) REFERENCES `data_objek` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

/*Data for the table `centroid_temp` */

insert  into `centroid_temp`(`id`,`id_kecamatan`,`iterasi`,`c1`,`c2`,`c3`) values (1,1,1,'0','1','0'),(2,2,1,'1','0','0'),(3,3,1,'1','0','0'),(4,4,1,'1','0','0'),(5,5,1,'0','1','0'),(6,6,1,'1','0','0'),(7,7,1,'1','0','0'),(8,8,1,'1','0','0'),(9,9,1,'1','0','0'),(10,10,1,'1','0','0'),(11,11,1,'1','0','0'),(12,12,1,'0','0','1'),(13,13,1,'0','1','0'),(14,14,1,'1','0','0'),(15,15,1,'1','0','0'),(16,16,1,'0','1','0'),(17,17,1,'1','0','0'),(18,18,1,'1','0','0'),(19,19,1,'0','1','0'),(20,20,1,'1','0','0'),(21,21,1,'0','1','0'),(22,22,1,'0','1','0'),(23,23,1,'0','1','0'),(24,24,1,'0','1','0'),(25,25,1,'0','1','0'),(26,26,1,'0','1','0'),(27,27,1,'1','0','0'),(28,28,1,'1','0','0'),(29,29,1,'0','1','0'),(30,30,1,'0','1','0'),(31,1,2,'0','1','0'),(32,2,2,'1','0','0'),(33,3,2,'1','0','0'),(34,4,2,'1','0','0'),(35,5,2,'0','1','0'),(36,6,2,'1','0','0'),(37,7,2,'1','0','0'),(38,8,2,'1','0','0'),(39,9,2,'1','0','0'),(40,10,2,'1','0','0'),(41,11,2,'1','0','0'),(42,12,2,'0','0','1'),(43,13,2,'0','1','0'),(44,14,2,'1','0','0'),(45,15,2,'1','0','0'),(46,16,2,'0','1','0'),(47,17,2,'1','0','0'),(48,18,2,'1','0','0'),(49,19,2,'0','1','0'),(50,20,2,'1','0','0'),(51,21,2,'0','1','0'),(52,22,2,'0','1','0'),(53,23,2,'0','1','0'),(54,24,2,'0','1','0'),(55,25,2,'0','1','0'),(56,26,2,'0','1','0'),(57,27,2,'1','0','0'),(58,28,2,'1','0','0'),(59,29,2,'0','1','0'),(60,30,2,'0','1','0');

/*Table structure for table `data_objek` */

DROP TABLE IF EXISTS `data_objek`;

CREATE TABLE `data_objek` (
  `id` int(5) NOT NULL,
  `kecamatan` varchar(20) NOT NULL,
  `nilai_mop` int(11) NOT NULL,
  `nilai_kondom` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_objek` */

insert  into `data_objek`(`id`,`kecamatan`,`nilai_mop`,`nilai_kondom`) values (1,'Cisalak',303,186),(2,'Sukasari',18,62),(3,'Pamanukan',4,64),(4,'Kasomalang',47,10),(5,'Cijambe',276,10),(6,'Jalancagak',57,0),(7,'Ciater',6,10),(8,'Serang Panjang',45,32),(9,'Sagalaherang',68,36),(10,'Pagaden',29,107),(11,'Pagaden Barat',21,49),(12,'Ciasem',473,385),(13,'Subang',125,147),(14,'Dawuan',24,31),(15,'Cibogo',20,57),(16,'Cipunagara',185,72),(17,'Purwadadi',47,53),(18,'Cikaum',76,0),(19,'Kalijati',34,139),(20,'Cipeundeuy',27,67),(21,'Tanjung Siang',96,249),(22,'Compreng',84,193),(23,'Pusakajaya',101,138),(24,'Blanakan',85,231),(25,'Binong',23,192),(26,'Tambakdahan',10,232),(27,'Patokbeusi',62,85),(28,'Pabuaran',64,100),(29,'Pusakanagara',117,269),(30,'Legonkulon',57,191),(31,'dd',333,11),(32,'dd',333,11);

/*Table structure for table `hasil_centroid` */

DROP TABLE IF EXISTS `hasil_centroid`;

CREATE TABLE `hasil_centroid` (
  `nomor` int(2) NOT NULL AUTO_INCREMENT,
  `c1a` varchar(50) NOT NULL,
  `c1b` varchar(50) NOT NULL,
  `c2a` varchar(50) NOT NULL,
  `c2b` varchar(50) NOT NULL,
  `c3a` varchar(50) NOT NULL,
  `c3b` varchar(50) NOT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `hasil_centroid` */

insert  into `hasil_centroid`(`nomor`,`c1a`,`c1b`,`c2a`,`c2b`,`c3a`,`c3b`) values (1,'4','64','84','193','473','385'),(2,'38.4375','47.6875','115.07692307692','173','473','385'),(3,'38.4375','47.6875','115.07692307692','173','473','385');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
